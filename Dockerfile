# our base image
FROM kuhlskev/ansible_host
MAINTAINER Kevin Kuhls <kekuhls@cisco.com>

# Copy the files over

WORKDIR /home/docker
COPY . /home/docker/